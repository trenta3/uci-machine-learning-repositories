#!/bin/bash

MAIN_URL="https://archive.ics.uci.edu/ml/machine-learning-databases"
DATAFOLDER="datasets"

# $1 is the path dir/file relative to mld
download_file () {
    echo "Downloading file $1"
    wget -O "$DATAFOLDER/$1" "$MAIN_URL/$1" 2>/dev/null
}

# $1 is the directory relative to machine-learning-databases to download
download_directory () {
    echo "Downloading directory $1"
    files=$(curl "$MAIN_URL/$1" 2>/dev/null | grep -oP "(?<=href\=\")[A-Za-z\_\-\.0-9\/]+" | grep -v "^\/")

    mkdir -p "$DATAFOLDER/$1"
    for file in $files; do
	if [[ $file =~ /\/$/ ]]; then
	    download_directory "$1$file"
	else
	    download_file "$1$file"
	fi
    done
}

directories=$(curl "$MAIN_URL/" 2>/dev/null | grep -oP "(?<=href\=\")[a-z][a-z0-9\-\_\.\/]+" | grep -v "^\/")
for directory in $directories; do
    echo "--> Downloading dataset $directory"
    download_directory "$directory"
done
